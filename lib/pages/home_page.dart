import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:prak2/models/oneday.dart';
import 'package:prak2/models/weather_model.dart';
import 'package:prak2/pages/weather_details.dart';
import 'package:prak2/services/weather_service.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
// api key
  late String _backgroundImage = '';
  final _weatherService = WeatherService("ab035ac16bfcc10640dbeae1bb93070d");
  TextEditingController textCityController = TextEditingController();
  Weath? _weather;
  Forecast? _forecast;
  final List<ListElement> _forecastList = [];

// Получаем прогноз погоды

  Future<void> _fetchWeather(String cityName) async {
// получить погоду для этого города
    try {
      final weather = await _weatherService.getWeather(cityName);
      setState(() {
        _weather = weather;
      });
    } catch (e) {
      print("cant get weather");
      print(e);
    }
  }

  Future<void> _fetchForecast(String cityName) async {
    try {
      final forecast = await _weatherService.getForecast(cityName);
      setState(() {
        _forecast = forecast;
        _setForecastList();
      });
    } catch (e) {
      print("cant get forecast");
      print(e);
    }
  }

  void _setForecastList() async {
    _forecastList.clear();
    _forecastList.addAll(_forecast!.list.where((e) => e.dtTxt.toString().contains("12:00:00.")).toList());
  }

  @override
  void initState() {
    super.initState();
// получение погоды при запуске
    _setBackgroundImage();
    init().then((value) => {
      setState((){
        textCityController.text = _weather?.cityName ?? "Город";
      })
    });
  }

  Future<void> init([String? cityName]) async {
    cityName ??= await _weatherService.getCurrentCity();
    await _fetchWeather(cityName!);
    await _fetchForecast(cityName);
  }

  void _setBackgroundImage() {
    var hourNow = int.parse(DateFormat('kk').format(DateTime.now()));
    if (hourNow >= 23 || hourNow < 4) {
      setState(() {
        _backgroundImage = 'assets/night.jpg';
      });
    } else if (hourNow >= 4 && hourNow < 12) {
      setState(() {
        _backgroundImage = 'assets/morning.jpg';
      });
    } else if (hourNow >= 12 && hourNow < 17) {
      setState(() {
        _backgroundImage = 'assets/noon.jpg';
      });
    } else if (hourNow >= 17 && hourNow < 23) {
      setState(() {
        _backgroundImage = 'assets/afternoon.jpg';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_weather != null) {
      return Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(0, 70, 0, 0),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(_backgroundImage),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              TextField(
                  controller: textCityController,
                  textAlign: TextAlign.center,
                  decoration: const InputDecoration(
                    border: InputBorder.none
                  ),
                  onSubmitted: (newCity){
                    setState(() async {
                      await init(newCity);
                    });
                  },
                  style: const TextStyle(
                      fontFamily: '.SF UI Text',
                      fontSize: 45,
                      fontWeight: FontWeight.bold,
                      color: Colors.purple)
                ),
              Stack(
                alignment: Alignment.topCenter,
                children: [
                  Lottie.asset("assets/${_weather?.icon ?? "01d"}.json", height: 225, width: 225),
                  Text(
                    '${_weather?.temperature.round()}°C',
                    style: const TextStyle(
                        fontFamily: '.SF UI Text',
                        fontSize: 50,
                        color: Colors.purple,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.italic
                    ),
                  )
                ],
              ),
              Text(_weather?.description ?? "Описание",
                  style: const TextStyle(
                      fontFamily: '.SF UI Text',
                      fontSize: 25,
                      fontStyle: FontStyle.italic,
                      color: Colors.purple
                  )
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.asset('assets/gauge.png', height: 50, width: 50),
                  Text(
                      '${_weather?.pressure.round().toString()} мм.рт.с.' ??
                          "Давление",
                      style: const TextStyle(color: Colors.purple)),
                  Image.asset('assets/wind.png', height: 50, width: 50),
                  Text('${_weather?.speed} м/с' ?? "Скорость ветра",
                      style: const TextStyle(color: Colors.purple)),
                  Image.asset('assets/humidity.png', height: 50, width: 50),
                  Text('${_weather?.humidity}%' ?? "Влажность",
                      style: const TextStyle(color: Colors.purple)),
                ],
              ),
              Expanded(
                child: ListView.separated(
                    padding: const EdgeInsets.only(
                        top: 18,
                        left: 22,
                        right: 22
                    ),
                    itemCount: _forecastList.length,
                    itemBuilder: (context, index) {
                      var elem = _forecastList[index];
                      return GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => WeatherDetailsPage(element: elem)));
                        },
                        child: SizedBox(
                          width: double.infinity,
                          height: 50,
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(
                                    elem.formatDate(),
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.purple
                                    ),
                                  )
                              ),
                              Expanded(
                                child: Lottie.asset("assets/${elem.weather[0].icon ?? "01d"}.json", height: 50, width: 50),
                              ),
                              Expanded(
                                  child: Text(
                                    "${elem.main!.temp!.round()}°C",
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.purple
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(height: 18);
                  }),
              )
            ]),
          ),
        ),
      );
    } else {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }
}

// Класс "HomePage" является изменяемым виджетом состояния и наследуется от класса
//  "StatefulWidget". В конструкторе класса указывается ключ 'key' и используется
//  вызов конструктора суперкласса.

// Класс "_HomePageState" является состоянием виджета "HomePage" и наследуется от
// класса "State". В этом классе определены методы и переменные для получения и
// отображения данных о погоде.

// Метод "_fetchWeather" асинхронно получает текущий город и погодные данные с
// использованием экземпляра класса "WeatherService". После получения данных,
// метод обновляет состояние виджета с помощью метода "setState". Если возникают
// ошибки, исключение обрабатывается и выводится сообщение об ошибке.

// Метод "initState" вызывается при инициализации виджета и выполняет получение
// данных о погоде с помощью метода "_fetchWeather".

// Метод "build" строит виджет на основе полученных данных о погоде. Если данные
// присутствуют, создается экземпляр "Scaffold" с разметкой для отображения погоды.
//  В противном случае отображается индикатор загрузки. Виджет включает в себя текст
//   с названием города и описанием погоды, а также изображения и текст с данными
//   о давлении, скорости ветра и влажности.

// Таким образом, этот код реализует виджет "HomePage", который получает и
// отображает данные о погоде.
