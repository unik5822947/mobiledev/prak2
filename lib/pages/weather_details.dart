import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:prak2/models/oneday.dart';

class WeatherDetailsPage extends StatefulWidget {

  final ListElement element;

  const WeatherDetailsPage({super.key, required this.element});

  @override
  State createState() => _WeatherDetailsPageState();
}

class _WeatherDetailsPageState extends State<WeatherDetailsPage> {
  late String _backgroundImage = '';

  @override
  void initState() {
    super.initState();
    _setBackgroundImage();
  }

  void _setBackgroundImage() {
    var hourNow = int.parse(DateFormat('kk').format(DateTime.now()));
    if (hourNow >= 23 || hourNow < 4) {
      setState(() {
        _backgroundImage = 'assets/night.jpg';
      });
    } else if (hourNow >= 4 && hourNow < 12) {
      setState(() {
        _backgroundImage = 'assets/morning.jpg';
      });
    } else if (hourNow >= 12 && hourNow < 17) {
      setState(() {
        _backgroundImage = 'assets/noon.jpg';
      });
    } else if (hourNow >= 17 && hourNow < 23) {
      setState(() {
        _backgroundImage = 'assets/afternoon.jpg';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        body: Container(
          padding: const EdgeInsets.fromLTRB(0, 70, 0, 0),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(_backgroundImage),
              fit: BoxFit.cover,
            ),
          ),
          child: Center(
            child:
            Column(mainAxisAlignment: MainAxisAlignment.start, children: [
              Text(widget.element.formatDate(),
                  style: const TextStyle(
                      fontFamily: '.SF UI Text',
                      fontSize: 45,
                      fontWeight: FontWeight.bold,
                      color: Colors.purple)),
              Stack(
                alignment: Alignment.topCenter,
                children: [
                  Lottie.asset(
                      "assets/${widget.element.weather[0].icon ?? "01d"}.json",
                      height: 225,
                      width: 225
                  ),
                  Text(
                    '${widget.element.main!.temp!.round()}°C',
                    style: const TextStyle(
                        fontFamily: '.SF UI Text',
                        fontSize: 50,
                        color: Colors.purple,
                        fontWeight: FontWeight.w600,
                        fontStyle: FontStyle.italic),
                  )
                ],
              ),
              Text(widget.element.weather[0].description ?? "Описание",
                  style: const TextStyle(
                      fontFamily: '.SF UI Text',
                      fontSize: 25,
                      fontStyle: FontStyle.italic,
                      color: Colors.purple)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Image.asset('assets/gauge.png', height: 50, width: 50),
                  Text(
                      '${widget.element.main!.pressure!.round().toString()} мм.рт.с.' ??
                          "Давление",
                      style: const TextStyle(color: Colors.purple)),
                  Image.asset('assets/wind.png', height: 50, width: 50),
                  Text('${widget.element.wind!.speed} м/с' ?? "Скорость ветра",
                      style: const TextStyle(color: Colors.purple)),
                  Image.asset('assets/humidity.png', height: 50, width: 50),
                  Text('${widget.element.main!.humidity}%' ?? "Влажность",
                      style: const TextStyle(color: Colors.purple)),
                ],
              ),
              const Expanded(
                  child: Center(
                    child: Text(
                      "Weather details",
                      style: TextStyle(
                        color: Colors.purple,
                        fontSize: 32,
                        fontWeight: FontWeight.w600
                      ),
                    ),
                  )
              )
            ]),
          ),
        ),
      );
  }
}